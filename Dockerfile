# This file is a template, and might need editing before it works on your project.
FROM python

WORKDIR /usr/src/app

ENV JAEGAR_HOST jaegar-jaeger-agent.kube-tracing.svc.cluster.local

ENV JAEGAR_PORT 5775

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

RUN chmod +x /usr/src/app/startup.sh

EXPOSE 5000

# For some other command
CMD ["sh","startup.sh"]
