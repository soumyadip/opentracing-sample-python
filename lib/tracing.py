import logging
import os
from jaeger_client import Config

def init_tracer(service):
    logging.getLogger('').handlers = []
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)
    #print(os.environ['JAEGAR_HOST'])
    #print(os.environ['JAEGAR_PORT'])
    host = os.environ['JAEGAR_HOST']
    port = os.environ['JAEGAR_PORT']
    print('host:' + host)
    print('port:' + port)
    #host = os.getenv['JAEGAR_HOST','jaegar-jaeger-agent.kube-tracing.svc.cluster.local']
    #port = os.getenv['JAEGAR_PORT',5775]
    config = Config(
        config={
            'sampler': {
                'type': 'const',
                'param': 1,
            },
            'local_agent': {
                'reporting_host': host,
                'reporting_port': port,
            },
            'logging': True,
        },
        service_name=service,
    )

    # this call also sets opentracing.tracer
    return config.initialize_tracer()